# Workstation Tools

This repository contains scripts to automate and speedup the workflow and preparation for my machine.

> **_Disclaimer_** :  
> Those scripts are ubuntu related with major version 18+, for other distributions you'll need to adapt it
___

## Prepare Workstation

> Read the `main.yml` file before applying and be sure to understand everything that will be done.

1. Clone this repository
```bash
https://gitlab.com/bergpb/ansible-prepare-workstation.git
```

2. Install system requirements:
```bash
sudo apt update && sudo apt install ansible unzip git -y
```

2. Apply the configuration
```bash
ansible-playbook main.yml --ask-become
```
>Type your password when asked to give root permissions for some actions.
___

# License
GPLv3
